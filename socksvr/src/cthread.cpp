#ifdef _WIN32
#include <windows.h>
#endif // _WIN32
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <time.h>
#include <errno.h>
#include "cthread.h"

bool CustomThread::Start(void * arg)
{
    Arg(arg);

    threadStatus = pthread_create(&threadID, NULL, CustomThread::EntryPoint, this);

    if(threadStatus == 0)
        return true;

    return false;
}

int CustomThread::Run(void *arg)
{
    Setup();
    Execute( arg );
    threadID = 0;
    threadStatus = 0;
}

void CustomThread::Sleep(int ms)
{
#ifdef _WIN32
    ::sleep( ms );
#else
    struct  timeval tv = {0};
    int     sel_err;

    do {
        errno = 0;

        tv.tv_sec = ms / 1000;
        tv.tv_usec = (ms % 1000) * 1000;

        sel_err = select(0, NULL, NULL, NULL, &tv);
    }while ( sel_err && (errno = EINTR) );
#endif
}

void CustomThread::SleepUM(int ums)
{
#ifdef _WIN32
    ::sleep( ums / ( 1000  * 1000 ) );
#else
    struct  timeval tv = {0};
    int     sel_err;

    do {
        errno = 0;

        tv.tv_sec = ums / ( 1000  * 1000 );
        tv.tv_usec = ums % ( 1000 * 1000 );

        sel_err = select(0, NULL, NULL, NULL, &tv);
    }while ( sel_err && (errno = EINTR) );
#endif
}

void* CustomThread::EntryPoint(void *pthis)
{
    if(pthis)
    {
        CustomThread *pt = (class CustomThread*)pthis;


        if ( pt != NULL )
        {
            pt->Run( pt->Arg() );
        }
    }
}
