#ifdef _WIN32
    #include <windows.h>
#endif // _WIN32

#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include "acqproc.h"

class MyClass : public AcquireProcessorEventHandler
{
    public:
        MyClass() : myacqproc( NULL )
        {
            myacqproc = GetAcquireProcessor();
            if ( myacqproc != NULL )
            {
                myacqproc->eventHandler = this;

                // Registering reserved command => Not work now.
            }
        }

        ~MyClass()
        {
            myacqproc = NULL;
        }

    public:
        int Run()
        {
            int reti = 0;

            if ( myacqproc != NULL )
            {
                while( myacqproc->GetRunningState() )
                {
                    // Just nothing to here, wait for ya.
                    usleep( 1000 );
                }
            }

            return reti;
        }


    public: /// inherited by AcquireProcessorEventHandler
        void OnClinetConnect()
        {
            printf("A client connected.\n");
            // Send hello to client.
            myacqproc->sendCmd("HELLO?\n");
        }

        void OnClientDisconnect()
        {
            printf("A client disconnected.\n");
        }

        void OnRecv(int size)
        {
            printf("Data received : %d bytes\n", size );

            // Let's Make it echo.
            string resrvcmd = myacqproc->GetReceiveBuff();

            if ( resrvcmd.find( "QUIT" ) == 0 )
            {
                // May stops all thread ..
                myacqproc->sendCmd("OK:QUIT\n");
                // Let's disconnect client.
                myacqproc->closeClient();

            }
            else
            {
                myacqproc->sendCmd( resrvcmd.c_str() );
                myacqproc->Sleep( 1 );
            }

            myacqproc->flushRcvBuff();
        }

        void OnReserveRecv(const char* data, const int size)
        {
            if ( ( data != NULL ) && ( size > 0 ) )
            {
                myacqproc->flushRcvBuff();
            }
        }

        void OnError(int code, const char* msg)
        {
            printf("Error occured (%d) : %s\n", code, msg );
        }

    protected:
        AcquireProcessor* myacqproc;
};

int main( int argc, char** argv )
{
	printf( "Test async socket server for Rasberry Pi3x, by Raph.K.\n" );

    int reti = 0;

    InitAcquireProcessor();

    MyClass myclass;

    while( true )
    {
        reti = myclass.Run();
    }

    FinalAcquireProcessor();

	printf( "Ended.\n" );

    return reti;
}

