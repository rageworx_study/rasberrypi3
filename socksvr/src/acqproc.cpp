#ifdef _WIN32

#include <winsock2.h>
#include <windows.h>
#include <time.h>
#include <errno.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <cstring>

#endif

#include "acqproc.h"

////////////////////////////////////////////////////////////////////////////////

#define SOCKET_PORT         20300
#define SOCKET_BACKLOG      5

#define EVENT_STR_RECV      "ON_RECV"

#ifdef _WIN32
#define CLOSESOCKET( _fd_ )     closesocket( _fd_ )
#else
#define CLOSESOCKET( _fd_ )     close( _fd_ )
#endif

#ifdef _WIN32
#define sock_size           int
#else
#define sock_size           socklen_t
#endif

#ifdef _WIN32
#define _EWOULDBLOCK        WSAEWOULDBLOCK
#else
#define _EWOULDBLOCK        EWOULDBLOCK
#endif

////////////////////////////////////////////////////////////////////////////////

static AcquireProcessor*    acqProcInst = NULL;

#ifdef _WIN32
static WSADATA              wsData      = {0};
static bool                 wsDataSet   = false;
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

AcquireProcessor::AcquireProcessor()
 : running(false),
   accessingbuffer(false),
   cmdreserved(false),
   threadkeepalive(false),
   currentState(0),
   eventHandler(NULL)
{
#ifdef _WIN32
    if ( wsDataSet == false )
    {
        if( WSAStartup(0x0202, &wsData) != 0 )
        {
            WSACleanup();

            lastErrCode = -1;
            lastErrMsg  = "winsock not initialized";

            return;
        }

        wsDataSet = true;
    }
#endif

    // start thread for a TCP/IP socket server ...
    Start(this);
}

AcquireProcessor::~AcquireProcessor()
{
    if ( cli_fd >= 0 )
    {
        CLOSESOCKET( cli_fd );
    }

    if ( listen_fd >= 0 )
    {
        CLOSESOCKET( listen_fd );
    }

    if ( running == true )
    {
        pthread_cancel(GetHandle());
    }

#ifdef _WIN32
    if ( wsDataSet == true )
    {
        WSACleanup();
        wsDataSet = false;
    }
#endif
}

bool AcquireProcessor::sendCmd(const char* cmdStr)
{
    if ( ( listen_fd >= 0 ) && ( cli_fd >= 0 ) )
    {
        send(cli_fd, cmdStr, strlen(cmdStr), 0);

        return true;
    }

    return false;
}

bool AcquireProcessor::checkRcvCmd(const char* cmdStr)
{
    // first wait for buffer ready ...
    while ( accessingbuffer == false )
    {
        // wait for 1ms...
        Sleep(1);
    }

    if ( rcvBuffStr.size() > 0 )
    {
        size_t strpos = rcvBuffStr.find(cmdStr);

        if ( strpos != string::npos )
        {
            return true;
        }
    }

    lastErrCode = 0;
    lastErrMsg.clear();

    return false;
}

bool AcquireProcessor::reserveRcvCmnd(const char* cmdStr)
{
    reservedCmdStr = cmdStr;

    if ( cmdStr != NULL )
    {
        cmdreserved = true;
    }
    else
    {
        cmdreserved = false;
    }

    return true;
}

void AcquireProcessor::closeClient()
{
    if ( cli_fd >= 0 )
    {
        CLOSESOCKET( cli_fd );
    }
}

void AcquireProcessor::flushRcvBuff()
{
    rcvBuffStr.clear();
}

void AcquireProcessor::Setup()
{
    threadkeepalive = true;
}

void AcquireProcessor::Execute(void*)
{
    bool server_run = true;
    bool close_conn = false;
    int  desc_ready = 0;

    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if ( listen_fd < 0 )
    {
        lastErrCode = listen_fd;
        lastErrMsg  = "failed to creating socket.";
        return;
    }

    int on = 1;
    int rc =  setsockopt(listen_fd, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
    if ( rc < 0 )
    {
        lastErrCode = rc;
        lastErrMsg  = "failed to set socket option : SO_REUSEADDR";
        CLOSESOCKET( listen_fd );
        return;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port        = htons(SOCKET_PORT);
    rc = bind(listen_fd, (struct sockaddr *)&addr, sizeof(addr));
    if ( rc < 0 )
    {
        lastErrCode = rc;
        lastErrMsg  = "socket binding failed";
        CLOSESOCKET( listen_fd );
    }

    rc = listen(listen_fd, 5);
    if ( rc < 0 )
    {
        lastErrCode = rc;
        lastErrMsg  = "socket listen failed";
        CLOSESOCKET( listen_fd );
    }


    while ( threadkeepalive == true )
    {
        server_run = true;

        while( server_run == true )
        {
            sock_size len_cli = sizeof(addr_cli);

            cli_fd = accept( listen_fd, (struct sockaddr*)&addr_cli, &len_cli);
            if( cli_fd < 0 )
            {
                if ( errno != _EWOULDBLOCK )
                {
                    // failed to accept client...
                    cli_fd = -1;
                    break;
                }

            }
            else
            {
                close_conn = false;

                if ( eventHandler != NULL )
                {
                    eventHandler->OnClinetConnect();
                }

                while ( close_conn == false )
                {
                    rc = recv(cli_fd, tempbuffer, ACQPROC_TEMP_BUFFER_SIZE, 0);
                    if ( rc <= 0 )
                    {
                        if ( errno != _EWOULDBLOCK )
                        {
                            // disconnected !!!
                            close_conn = true;
                            server_run = false;
                            CLOSESOCKET( cli_fd );
                            cli_fd = -1;

                            if ( eventHandler != NULL )
                            {
                                eventHandler->OnClientDisconnect();
                            }
                        }
                    }
                    else
                    if ( rc > 0 )
                    {
                        //rcvBuffStr += tempbuffer;
                        accessingbuffer = true;
                        rcvBuffStr.append(tempbuffer, rc);
                        accessingbuffer = false;

                        if ( eventHandler != NULL )
                        {
                            eventHandler->OnRecv(rc);
                        }

                    }

                    Sleep(1);

                } /// of while( -- )

            }
        }

        if ( cli_fd >= 0 )
        {
            CLOSESOCKET( cli_fd );
            cli_fd = -1;

            if ( eventHandler != NULL )
            {
                eventHandler->OnClientDisconnect();
            }
        }

    }

    if ( listen_fd >= 0 )
    {
        CLOSESOCKET( listen_fd );
        listen_fd = -1;
    }

    running = false;

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void InitAcquireProcessor()
{
    if ( acqProcInst == NULL )
    {
        acqProcInst = new AcquireProcessor();
    }
}

AcquireProcessor* GetAcquireProcessor()
{
    return acqProcInst;
}

void FinalAcquireProcessor()
{
    if ( acqProcInst != NULL )
    {
        delete acqProcInst;
        acqProcInst = NULL;
    }
}
