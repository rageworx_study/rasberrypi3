#ifndef __ACQPROC_H__
#define __ACQPROC_H__

#include <string>

using namespace std;

#ifndef _WIN32
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#endif

#include "cthread.h"


#define ACQPROC_TEMP_BUFFER_SIZE        1024

#ifdef _WIN32
    #define SOCKFD  SOCKET
#else
    #define SOCKFD  int
#endif

// -----------------------------------------------------------------------------
// a interface

class AcquireProcessorEventHandler
{
    public:
        virtual void OnClinetConnect() = 0;
        virtual void OnClientDisconnect() = 0;
        virtual void OnRecv(int size) = 0;
        virtual void OnReserveRecv(const char* data, const int size) = 0;
        virtual void OnError(int code, const char* msg) = 0;
};

// -----------------------------------------------------------------------------

class AcquireProcessor
 : public CustomThread
{
    public:
        AcquireProcessor();
        virtual ~AcquireProcessor();

    public:
        bool sendCmd(const char* cmdStr);
        bool checkRcvCmd(const char* cmdStr);
        bool reserveRcvCmnd(const char* cmdStr);
        void closeClient();
        void flushRcvBuff();

    public:
        AcquireProcessorEventHandler* eventHandler;

    public:
        int         GetReceiveBuffFilledSize() { return rcvBuffStr.size(); }
        const char* GetReceiveBuff() { return rcvBuffStr.data(); }
        const char* GetLastErrorMsg() { return lastErrMsg.c_str(); }
        const int   GetLastErrorCode() { return lastErrCode; }
        const bool  GetRunningState() { return (const bool)running; }
        void        SendThreadStopSig() { threadkeepalive = false; }

    protected:
        char        tempbuffer[ACQPROC_TEMP_BUFFER_SIZE];
        int         currentState;
        string      reservedCmdStr;
        string      rcvBuffStr;
        string      lastErrMsg;
        int         lastErrCode;
        bool        running;
        bool        accessingbuffer;
        bool        cmdreserved;
        bool        threadkeepalive;

    public:
        void Setup();
        void Execute(void*);

    private:
        SOCKFD      listen_fd;
        SOCKFD      max_fd;
        SOCKFD      cli_fd;

        struct sockaddr_in  addr;
        struct sockaddr_in  addr_cli;
        struct timeval      timeout;
        fd_set              master_fdset;
        fd_set              working_fdset;
};

void InitAcquireProcessor();
AcquireProcessor* GetAcquireProcessor();
void FinalAcquireProcessor();

#endif /// of __ACQPROC_H__
