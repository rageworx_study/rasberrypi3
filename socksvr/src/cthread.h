////////////////////////////////////////////////////////////////////////////////
// CustomThread for POSIX thread
//                                                           .... version 0.2
// ============================================================================
// (C)Copyright 2011-2013 , rage.kim@vatech.co.kr
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __CUSTOMTHREAD_H__
#define __CUSTOMTHREAD_H__

#include <pthread.h>

typedef void (*PTHREADFUNC)(void* pParam);
typedef PTHREADFUNC     PBEGINTHREADEX_THREADFUNC;

class CustomThread
{
    public:
        CustomThread() {};

    public:
        bool Start(void * arg);
        pthread_t GetHandle() { return threadID; }

    public:
        void Sleep(int ms);
        void SleepUM(int ums);

    protected:
        static void* EntryPoint(void*);

    protected:
        int Run(void * arg);
        virtual void Setup() {};
        virtual void Execute(void*) {};

        void *Arg() const {return Arg_;}
        void Arg(void* a){Arg_ = a;}

    private:
        pthread_t   threadID;
        int         threadStatus;
        void       *Arg_;
};

#endif // __CUSTOMTHREAD_H__
