# A repository for Rasberry Pi 3 study and utilities

## What contains ?

* Some studying projects.
* Some modified, ported libraries.
* Some utility program source codes.
* Some daemons

## Cross compilation platform for easily approach.

* Windows 64bit ( formally windows 10 )
* MSYS2 with zsh ( Oh-My-Zsh )

## Cross compiler for Windows

* Download latest version of compiler from [here](http://gnutoolchains.com/raspberry/).
* Recommned to download "rasberry-gcc6.3.0.exe" or higher version if exists.
* Just install downloaded package to your Windows ( It takes a lot of minutes ).
* Test cross compiler works on Windows in command shell or MSYS2 bash or zsh.
* This is an example for command shell:

```
C:\Users\You> cd C:\SysGCC\rasberry\bin
C:\SysGCC\rasberry\bin> arm-linux-gnueabihf-gcc --version
arm-linux-gnueabihf-gcc.exe (Raspbian 6.3.0-18+rpi1) 6.3.0 20170516
Copyright (C) 2016 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

* When should see version of gcc, it may done for first job.

## Using MSYS for bash.

* Strongly recommend to use MSYS2 and zsh shell for your enhanced compiling environments.
* It shoudl be needed download MSYS2, and install then some more configuration may required.

## Using WinSCP for SFTP.

* [WinSCP](https://winscp.net) will help to transfer compiled ELF binary to your Rasberry Pi3.
* Just [download WinSCP](https://winscp.net/eng/download.php) and install it.

## Emulator QEMU on Windows.

* Source code and download [here](https://sourceforge.net/projects/rpiqemuwindows)
